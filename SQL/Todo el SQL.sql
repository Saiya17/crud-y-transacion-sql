-- CREACION DE LAS TABLAS 
CREATE TABLE producto (
id_p INT IDENTITY PRIMARY KEY NOT NULL,
codigo VARCHAR(10),
descripcion VARCHAR(MAX),
precio MONEY,
cantidad_p INT
)
GO

ALTER TABLE producto ADD CHECK (cantidad_p>=0)

CREATE TABLE venta(
id_v INT IDENTITY (1,1) PRIMARY KEY NOT NULL,
id_p INT REFERENCES producto NOT NULL,
cantidad INT,
fecha_v DATE
)
GO

ALTER TABLE venta ADD CHECK (cantidad>=0)

-- INGRESO DE LOS DATOS
INSERT INTO producto VALUES('E1','Martillo',6.00,45)
INSERT INTO producto VALUES('E2','Taladro',30.00,14)
INSERT INTO producto VALUES('E3','Rodillos de pintar',3.00,22)

-- PROCEDIMIENTOS ALMACENADOS - MODO TRANSACIONAL
CREATE PROC PA_REALIZAR_VENTA_TRANSACIONAL (
@id_v INT,
@id_p1 INT,
@cantidad INT,
@fecha DATE,
@mensaje VARCHAR (50) OUTPUT)
    AS
BEGIN	
    BEGIN TRY
        BEGIN TRANSACTION
		IF(EXISTS(SELECT * FROM venta WHERE id_v =@id_v ))
        SET @mensaje ='VENTA YA  EXISTE'
	    ELSE
        IF(EXISTS(SELECT * FROM producto WHERE cantidad_p<@cantidad))
	    SET @mensaje ='NO HAY SUFICIENTE STOCK'
        INSERT INTO venta VALUES (@id_p1, @cantidad, @fecha)
        UPDATE producto SET cantidad_p=cantidad_p - @cantidad WHERE id_p=@id_p1
        SET @mensaje= 'VENTA REALIZADA CORRECTAMENTE'
        COMMIT TRANSACTION
    END TRY
	BEGIN CATCH
	    ROLLBACK TRANSACTION
        RAISERROR ('No se pudo realizar la venta',1,2);
    END CATCH
END    
GO


-- PROCEDIMIENTOS ALMACENADOS - MODO NO TRANSACIONAL
CREATE PROC PA_REALIZAR_VENTA_NO_TRANSACIONAL (
@id_v INT,
@id_p1 INT,
@cantidad INT,
@fecha DATE,
@mensaje VARCHAR (50) OUTPUT
)
AS
BEGIN
IF(EXISTS(SELECT * FROM venta WHERE id_v =@id_v ))
SET @mensaje ='VENTA YA  EXISTE'
ELSE
IF(EXISTS(SELECT * FROM producto WHERE cantidad_p<@cantidad))
SET @mensaje ='NO HAY SUFICIENTE STOCK'
INSERT INTO venta VALUES (@id_p1, @cantidad, @fecha)
UPDATE producto SET cantidad_p=cantidad_p - @cantidad WHERE id_p=@id_p1
SET @mensaje= 'VENTA REALIZADA CORRECTAMENTE'
END 


-- EJECUTOR DE LOS PROCEDIMIENTOS ALMACENADOS **TRANSACCIONAL**
DECLARE @mensaje VARCHAR(50)
EXEC PA_REALIZAR_VENTA_TRANSACIONAL '','22','10','4/5/2021', @mensaje OUTPUT
SELECT @mensaje

-- EJECUTOR DE LOS PROCEDIMIENTOS ALMACENADOS
DECLARE @mensaje VARCHAR(50)
EXEC PA_REALIZAR_VENTA_NO_TRANSACIONAL '','22','10','4/5/2021', @mensaje OUTPUT
SELECT @mensaje

-- CONSULTAS SQL
SELECT * FROM venta 
SELECT * FROM producto

-- ELIMINAR REGISTROS 
delete from producto
delete from venta
